<?php
class Reponse{

   // la réponse 
    private $titre;

    // false si la réponse est fausse et true si elle est correcte
    private $value;
    
    Const BONNE_REPONSE ="true";

    //Construteur, si le deuxiéme argument est false par défaut s'il n'est pas specifié
    public function __Construct($reponse,$value="false")
    {
        $this->titre=$reponse;
        $this->value=$value;

    }

    public function setTitre($titre)
	{
		$this->titre=$titre;
    }

    public function getTitre()
	{
		return $this->titre;
    }

    public function setValue($value)
	{
		$this->value=$value;
    }
    public function getValue()
	{
		return $this->value;
    
    }
    
}
?>