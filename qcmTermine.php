<?php

require("Qcm.php");
require("Question.php");
require("Reponse.php");
$qcm = new Qcm();
session_start();

// si le formulaire est validé
if(isset($_POST['ajouter']))
{

     for ($j=1; $j <=$_SESSION['Nb'] ; $j++) { 
         
        //création de la question
        $question1 = new Question($_POST['question'.$j]);

        for ($i=1; $i <5 ; $i++) { 

             if($_POST['choix'.$j]=='reponse'.$i.$j)
                // si cest la reponse correcte , on passe le const BONNE_REPONSE par argument
                $question1->ajouterReponse(new Reponse($_POST['reponse'.$i.$j], Reponse::BONNE_REPONSE));
            else
                // si ce n'est pas la reponse correcte, on passe juste le premier argument le deuxième c'est false par défaut
                $question1->ajouterReponse(new Reponse($_POST['reponse'.$i.$j]));
}
// ajouter les eplication à la questions
$question1->setExplications($_POST['explication'.$j]);

//ajouter la question au qcm
$qcm->ajouterQuestion($question1);
}
// ajouter les appreciations qcm
$qcm->setAppreciation(array('0-5'=>$_POST['app1'],'6-10'=>$_POST['app2'],'11-15'=>$_POST['app3'],'16-19'=>$_POST['app4'],'20'=>$_POST['app5']));



}



?>

<!DOCTYPE html>
        <html>
            <head>
                <title>QSM</title>
                <style type=text/css>
                    
                    div {
                    text-align:center;
					margin-left:20%;
					margin-right:20%;
                    margin-top:10px;
                    margin-bottom:10px;
                     }
                     .question {
                    border: 10px solid orange;
                    border-width:3px;
                    border-color:orange;
                    background-color:orange;
                    margin-top:20px;

                     }
                     .reponse{
                    border: 10px solid lightskyblue;
                    border-width:3px;
                    border-color:lightskyblue;
                    background-color:lightskyblue;
                    }
                    .reponseCorrect{
                    border: 10px solid Aquamarine;
                    border-width:3px;
                    border-color:Aquamarine;
                    background-color:Aquamarine;
                    }
                    p{
                    margin-left:20%;
					margin-right:20%;
                    margin-top:5px;

                    }
                     </style>
            </head>
        <body>
            <div>
            <h2>votre Questionnaire est terminé</h2>
            <img src="end.png">
            
            </div>
            <?php
            $qcm->generer();

            ?>
        </body>

    </html>
