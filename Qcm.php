<?php
class Qcm{

    // tableau de questions
    private $questions;

    //tableau appreciation
    private $appreciation;

    //Constructeur
    public function __Construct()
    {
        $this->questions=array();
        $this->appreciation=array();
    }
    
    public function getQuestions()
    {
        return $this->questions;
    }
    public function setQuestions($questions)
    {
         $this->questions=$questions;
    }
    public function getAppreciation()
    {
        return $this->appreciation;
    }
    public function setAppreciation( $appreciation)
    {
        $this->appreciation=$appreciation;
        
    }
    // ajouter une question à un Qsm
    public function ajouterQuestion(Question $question)
    {
        array_push($this->questions, $question);
    }

    // Génerer un Qcm en l'affichant
    public function generer()
    {
        $output="";
        
        $nbQ=1;

        foreach($this->questions as $q) {

         $output.="<div class=question> Question ". $nbQ." :".$q->getTitre()."</div>";
         $nbQ++;
         $nbR=1;
         foreach($q->getReponses() as $r) {
            $color="reponse";
            if($r->getValue()=="true")
            $color="reponseCorrect";
           

            $output.="<div class=".$color." > Réponse ". $nbR." :".$r->getTitre()."</div>";
            $nbR++;
           }
           $output.="<p> Explications:  ".$q->getExplications()."<p>";

        }
        $output.="<div > Appreciation : </div>";

        foreach($this->getAppreciation() as $key=>$app) {
            $output.=" <div> ".$key." : ".$app."</div>";
        }
        echo count($this->getAppreciation());
        echo $output;
    }


    public function getQuestionFromMd5($md5) {
    foreach($this->questions as $q) {
        if(md5($q->getQuestion()) == $md5) {
            return $q;
        }
    }
    return FALSE;
    }

}
?>