<?php
class Question{

    // question
    private $titre;

    //tableau de reponses proposés
    private $reponses;

    //explication de la reponse correcte
	private $explications;

    //Construteur
    public function __Construct($question)
    {
        $this->setTitre($question);
        $this->reponses=array();
    }

	public function setTitre($titre)
	{
		$this->titre=$titre;
    }
    public function getTitre()
	{
		return $this->titre;
    }

    public function setReponses($reponses)
	{
        $this->reponses=$reponses;
        
    }
    public function getReponses()
	{
       return  $this->reponses;
        
    }
    
    public function setExplications($explication)
	{
		$this->explications=$explication;
    }
    public function getExplications()
	{
		return $this->explications;
    }

    // ajouter une reponse à une question
    public function ajouterReponse(Reponse $reponse)
    {
        array_push($this->reponses, $reponse);

    }
}
?>